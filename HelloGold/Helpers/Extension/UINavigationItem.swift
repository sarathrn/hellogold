//
//  UINavigationItem.swift
//  HelloGold
//
//  Created by Sattin on 26/12/17.
//  Copyright © 2017 Sattin. All rights reserved.
//

import Foundation
import UIKit

extension UINavigationItem {
    
    func updateLogoFrame() {
        
        // New Logo Replaces the Old One
        addLogo()
    }
    
    // Add logo on Navigation View
    func addLogo() {
        
        let width = UIScreen.main.bounds.width                                                                 
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: width, height: 32))
        imageView.image = UIImage(named: "logo")
        imageView.clipsToBounds = true
        imageView.contentMode = .scaleAspectFit
        self.titleView = imageView
    }
}
