//
//  Notification.swift
//  HelloGold
//
//  Created by Sattin on 27/12/17.
//  Copyright © 2017 Sattin. All rights reserved.
//

import Foundation

extension Notification.Name {
   
    static let kApplicationWillEnterForeground = Notification.Name("ApplicationWillEnterForeground")
}
