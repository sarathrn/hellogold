//
//  UIView.swift
//  HelloGold
//
//  Created by Sattin on 22/12/17.
//  Copyright © 2017 Sattin. All rights reserved.
//

import Foundation
import UIKit
import Toast_Swift

extension UIView {
    
    // Show User Alert
    func showToastMessage(_ message: String) {
        
        makeToast(message, duration: kToastDurationMiddle, position: kToastPosition, style: kToastStyle)
    }
    
    // Add Constraints for View
    func addConstraintsWithFormat(_ format: String, views: UIView...) {
        
        var viewsDictionary = [String: UIView]()
        for (index, view) in views.enumerated() {
          
            let key = "v\(index)"
            view.translatesAutoresizingMaskIntoConstraints = false
            viewsDictionary[key] = view
        }
        
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: format, options: NSLayoutFormatOptions(), metrics: nil, views: viewsDictionary))
    }
}
