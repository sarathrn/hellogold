//
//  String.swift
//  HelloGold
//
//  Created by Sattin on 26/12/17.
//  Copyright © 2017 Sattin. All rights reserved.
//

import Foundation

extension String {
    
    /*
        Convert String to Date
        UTC Time
    */
    func toUTCDate() -> Date? {
    
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        return dateFormatter.date(from: self)
    }
}
