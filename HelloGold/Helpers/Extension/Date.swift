//
//  date.swift
//  HelloGold
//
//  Created by Sattin on 26/12/17.
//  Copyright © 2017 Sattin. All rights reserved.
//

import Foundation

extension Date {
    
    /*  Convert UTC to Local Data & Time
        Format - 3.24 AM
     */
    func toLocalTime() -> String? {
        
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.dateFormat = "h:mm a"
        return dateFormatter.string(from: self)
    }

    /*  To Local Time and Date
        Format - Today, Yesterday, xx/xx/xxxx etc
    */
    func toTimeWithDayPrecision() -> String? {
        
        let formatter = DateFormatter()
        formatter.timeStyle = .none
        formatter.dateStyle = .full
        formatter.doesRelativeDateFormatting = true
        return formatter.string(from: self)
    }
}
