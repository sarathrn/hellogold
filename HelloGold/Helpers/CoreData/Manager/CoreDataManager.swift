//
//  CoreDataManager.swift
//  HelloGold
//
//  Created by Sattin on 22/12/17.
//  Copyright © 2017 Sattin. All rights reserved.
//

import Foundation
import CoreData

class CoreDataManager: NSObject {
    
    
    // Declarations
    static let shared = CoreDataManager()
    var storeName: String = "CDMDefaultStore"
    
    // Contexts
    open lazy var mainContext: NSManagedObjectContext = {
        self.getManagedObjectContextWithType(NSManagedObjectContextConcurrencyType.mainQueueConcurrencyType)
    }()
    
    open lazy var backgroundContext: NSManagedObjectContext = {
        self.getManagedObjectContextWithType(NSManagedObjectContextConcurrencyType.privateQueueConcurrencyType)
    }()
    
    // Core
    lazy var persistentContainer: NSPersistentContainer = {
        
        let container = NSPersistentContainer(name: self.storeName)
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            
            if let error = error as NSError? {
                
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    
    
    
    
    
    
    // MARK: Life Cycle
    override init() {
        super.init()
        
        NotificationCenter.default.addObserver(self, selector: #selector(CoreDataManager.contextDidSaveContext(_:)), name: NSNotification.Name.NSManagedObjectContextDidSave, object: nil)
    }
    
    deinit {
        
        NotificationCenter.default.removeObserver(self)
    }
}





// MARK: - Support
extension CoreDataManager {
    
    
    // MARK : Supportive Methods
    func initiateStore(_ storeName: String) {
        
        self.storeName = storeName
    }
    
    
    // Fetch Context
    fileprivate func getManagedObjectContextWithType(_ type: NSManagedObjectContextConcurrencyType) -> NSManagedObjectContext {
        
        let managedObjectContext: NSManagedObjectContext!
        switch type {
            
        case NSManagedObjectContextConcurrencyType.privateQueueConcurrencyType:
            managedObjectContext = self.persistentContainer.newBackgroundContext()
            
        case NSManagedObjectContextConcurrencyType.mainQueueConcurrencyType:
            managedObjectContext = self.persistentContainer.viewContext
            
        default:
            managedObjectContext = self.persistentContainer.viewContext
        }
        return managedObjectContext
    }
}



// MARK: - Context saving notifications
extension CoreDataManager {
    
    // Call back function by saveContext, Support multi-thread
    @objc func contextDidSaveContext(_ notification: Notification) {
        
        let sender = notification.object as! NSManagedObjectContext
        if sender === self.mainContext {
            
            self.backgroundContext.perform {
                
                self.backgroundContext.mergeChanges(fromContextDidSave: notification)
            }
        }
        else if sender === self.backgroundContext {
            
            self.mainContext.perform {
                
                self.mainContext.mergeChanges(fromContextDidSave: notification)
            }
        }
    }
    
    func save() {
        
        let context = persistentContainer.viewContext
        context.perform {
            
            do {
                
                try context.save()
            }
            catch {
                
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
}

