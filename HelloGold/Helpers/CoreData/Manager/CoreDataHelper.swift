//
//  CoreDataHelper.swift
//  HelloGold
//
//  Created by Sattin on 26/12/17.
//  Copyright © 2017 Sattin. All rights reserved.
//

import Foundation

extension CDPrice {
    
    // Save the Entity in the Main Context.
    public func save() {
        
        CoreDataManager.shared.save()
    }
    
    // Create New
    static public func create() -> CDPrice {
        
        return CDPrice(context: CoreDataManager.shared.mainContext)
    }
}
