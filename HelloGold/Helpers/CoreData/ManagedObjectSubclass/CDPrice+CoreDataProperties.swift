//
//  CDPrice+CoreDataProperties.swift
//  HelloGold
//
//  Created by Sattin on 26/12/17.
//  Copyright © 2017 Sattin. All rights reserved.
//
//

import Foundation
import CoreData


extension CDPrice {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<CDPrice> {
        return NSFetchRequest<CDPrice>(entityName: "CDPrice")
    }

    @NSManaged public var buy: Float
    @NSManaged public var sell: Float
    @NSManaged public var timeStamp: Date?
    
    
    /*  Transient Property
        Support Section Wise Filtering
    */
    @objc public var sectionName: String? {
        get {
            guard let _timeStamp = timeStamp else {
                return nil
            }
            return _timeStamp.toTimeWithDayPrecision()
        }
    }
}
