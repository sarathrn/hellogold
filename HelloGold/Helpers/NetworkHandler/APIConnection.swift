//
//  APIConnection.swift
//  HelloGold
//
//  Created by Sattin on 21/12/17.
//  Copyright © 2017 Sattin. All rights reserved.
//

import Foundation

class URLs {
   
    static let shared = URLs()
    let kBaseUrl = "https://cws.hellogold.com/"
    let kSpotPrice = "api/v2/spot_price.json"
}


class APIKeys {
    
    static let shared = APIKeys()
    let data = "data"
}
