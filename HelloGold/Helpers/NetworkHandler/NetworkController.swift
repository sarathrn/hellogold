//
//  NetworkController.swift
//  HelloGold
//
//  Created by Sattin on 21/12/17.
//  Copyright © 2017 Sattin. All rights reserved.
//

import Foundation
import Alamofire

class NetworkController {
    
    // Declarations
    static let shared = NetworkController()
    let networkHelper = NetworkHelper.shared
    let urls = URLs.shared
    
    
    func get(_ url: String, params: [String: Any], completion: @escaping (_ networkStatus: Bool, _ response: Any?) -> ()) {
        
        
        // Validate Internet connection
        if !networkHelper.isConnectedToNetwork()  {
            
            completion(false, nil)
            return
        }
        
        
        // Prepare Network
        Alamofire.request(makeUrl(url: url), method: .get, parameters: params, encoding: JSONEncoding.default).validate().responseJSON { response in
            
            
            if logActivity {
                
                print("\n\(String(describing: response.request?.url))")
            }
            
            
            switch response.result {
                
            case .success:
                
                if let resultJson = response.result.value {
                    
                    if logActivity {
                        print(resultJson)
                    }
                    
                    completion(true, resultJson)
                    return
                }
                else {
                    
                    if logActivity {
                        
                        print("NetworkHandler reporting -> Error on result parsing")
                        print(String(data: response.data!, encoding: String.Encoding.utf8) ?? "Nothing")
                    }
                }
                completion(true, nil)
                return
                
            case .failure(let error):
                
                if logActivity {
                    
                    print(response.request ?? "Nothing")  // original URL request
                    print(response.response ?? "Nothing") // HTTP URL response
                    print(String(data: response.data!, encoding: String.Encoding.utf8) ?? "Nothing")     // server data
                    print(response.result)   // result of response serialization
                    print(error)
                }
                completion(true, nil)
                return
                
            }
            
        }
    }
    
    // crate url to network call
    func makeUrl(url: String) -> String {
        
        return urls.kBaseUrl + url
        
    }
    
}
