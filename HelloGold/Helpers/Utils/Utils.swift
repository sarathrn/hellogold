//
//  Utils.swift
//  HelloGold
//
//  Created by Sattin on 22/12/17.
//  Copyright © 2017 Sattin. All rights reserved.
//

import Foundation
import Toast_Swift

// Support Logging
let logActivity = true


// Support Toast
var kToastStyle: ToastStyle = {
    var style = ToastStyle()
    style.messageColor = UIColor.black
    style.backgroundColor = UIColor.white
    return style
}()
let kToastDurationShort = 1.0
let kToastDurationMiddle = 3.0
let kToastDurationLong = 6.0
let kToastPosition = ToastPosition.top
