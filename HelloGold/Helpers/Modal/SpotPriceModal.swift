//
//  SpotPriceModal.swift
//  HelloGold
//
//  Created by Sattin on 22/12/17.
//  Copyright © 2017 Sattin. All rights reserved.
//

import Foundation

class SpotPriceModal: Base {
    
    var result: String?
    var data: SpotPriceModal_Data?
    
    
    
    // Support Parse
    override init() {
        super.init()
    }
    
    init(_ values: Dictionary<String, Any>) {
        super.init()
        
        // Set Values to Modal and Save to DataBase
        setValuesForKeys(values)
        saveDataToDB()
    }
    
    
    
    // Support Parsing
    override func setValue(_ value: Any?, forKey key: String) {
        
        switch key {
            
        case APIKeys.shared.data:
            if let values = value as? Dictionary<String, Any> {
                self.data = SpotPriceModal_Data(values)
            }
           
        default:
            super.setValue(value, forKey: key)
        }
    }
    
    func saveDataToDB() {
        
        //  Validation - Invalidate data if sell or buy prices are nil
        guard let buy = self.data?.buy else {
            return
        }
        guard let sell = self.data?.sell else {
            return
        }
        
        /*  Save info to database
            Time Stamp saves to data base as it is, which is UTC format.
         */
        let price = CDPrice.create()
        price.buy = Float(truncating: buy)
        price.sell = Float(truncating: sell)
        price.timeStamp = self.data?.timestamp?.toUTCDate()
        price.save()
    }
    
}

class SpotPriceModal_Data: Base {
    
    var buy: NSNumber?
    var sell: NSNumber?
    var timestamp: String?
    
    // Support Parse
    override init() {
        super.init()
    }
    
    init(_ values: Dictionary<String, Any>) {
        super.init()
        setValuesForKeys(values)
    }
}
















