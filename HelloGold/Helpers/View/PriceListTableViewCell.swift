//
//  PriceListTableViewCell.swift
//  HelloGold
//
//  Created by Sattin on 26/12/17.
//  Copyright © 2017 Sattin. All rights reserved.
//

import UIKit

class PriceListTableViewCell: UITableViewCell {

    
    // Connection Objects
    @IBOutlet weak var buyLabel: UILabel!
    @IBOutlet weak var sellLabel: UILabel!
    @IBOutlet weak var timeStampLabel: UILabel!
    
    // Declarations
    static var identifier: String {
        return String(describing: self)
    }
    
    // Assign Data
    var data: CDPrice? {
        
        didSet {
            
            clear()
            if let buy = data?.buy {
            
                buyLabel.text = "\(buy)"
            }
            if let sell = data?.sell {
                
                sellLabel.text = "\(sell)"
            }
            if let dateAndTime = data?.timeStamp, let timeStamp = dateAndTime.toLocalTime() {
                
                timeStampLabel.text = timeStamp
            }
        }
    }
    
    
    
    
    
    // MARK: Life Cycle
    override func awakeFromNib() {
        super.awakeFromNib()
    
        configureView()
    }

    
    // MARK: Arrange View
    func configureView() {
        
        
    }

    
    // Reset UIElements
    func clear() {
        
        buyLabel.text = nil
        sellLabel.text = nil
        timeStampLabel.text = nil
    }
}
