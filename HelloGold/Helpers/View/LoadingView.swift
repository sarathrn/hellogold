//
//  LoadingView.swift
//  HelloGold
//
//  Created by Sattin on 26/12/17.
//  Copyright © 2017 Sattin. All rights reserved.
//

import Foundation
import UIKit

class LoadingView: UIView {
    
    // Declarations
    fileprivate var gradient = CAGradientLayer()
    
    
    // MARK: Life Cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        // Set Gradient frame
        gradient.frame = self.bounds
    }
    
    
    
    // Initiate Gradient
    private func addGradient() {
        
        gradient.colors = [UIColor.random().cgColor, UIColor.random().cgColor]
        gradient.startPoint = CGPoint(x:0.0, y:0.5)
        gradient.endPoint = CGPoint(x:1.0, y:0.5)
        gradient.drawsAsynchronously = true
        self.layer.addSublayer(gradient)
        
        animateGradient()
    }
    
    
    // Animate Color Change
    private func animateGradient() {
        
        let gradientChangeAnimation = CABasicAnimation(keyPath: "colors")
        gradientChangeAnimation.duration = 1.0
        gradientChangeAnimation.toValue = [UIColor.random().cgColor, UIColor.random().cgColor]
        gradientChangeAnimation.fillMode = kCAFillModeForwards
        gradientChangeAnimation.isRemovedOnCompletion = false
        gradientChangeAnimation.delegate = self
        gradient.add(gradientChangeAnimation, forKey: "colorChange")
    }

}


// MARK: Events
extension LoadingView {
    
    func startAnimate() {
        
        addGradient()
    }
    
    func stopAnimate() {
        
        gradient.removeFromSuperlayer()
        gradient = CAGradientLayer()
    }
}


// Support Continuos Animation
extension LoadingView: CAAnimationDelegate {
   
    func animationDidStop(_ anim: CAAnimation, finished flag: Bool) {
       
        if flag {
        
            gradient.colors = [UIColor.random().cgColor, UIColor.random().cgColor]
            animateGradient()
        }
    }
}
