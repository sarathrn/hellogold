//
//  EmptyTableViewCell.swift
//  HelloGold
//
//  Created by Sattin on 26/12/17.
//  Copyright © 2017 Sattin. All rights reserved.
//

import UIKit


class EmptyView: UIView {
    
    // Declarations
    let emptyTextLabel: UILabel = {
        let label = UILabel()
        label.text = "Price List is Empty!"
        label.textAlignment = .center
        return label
    }()
    
    // MARK: Life Cycle
    override init(frame: CGRect) {
        super.init(frame: frame)
     
        configureView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: Arrange View
    func configureView() {
        
        // Add
        addSubview(emptyTextLabel)
        
        // Layout
        addConstraintsWithFormat("H:|-16-[v0]-16-|", views: emptyTextLabel)
        addConstraintsWithFormat("V:|-24-[v0]-24-|", views: emptyTextLabel)
    }
}
