//
//  ViewController.swift
//  HelloGold
//
//  Created by Sattin on 21/12/17.
//  Copyright © 2017 Sattin. All rights reserved.
//

import UIKit
import CoreData

class PriceListViewController: UIViewController {
    
    // MARK: Connection Objects
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var loadingView: LoadingView!
    @IBOutlet weak var headersContainerView: UIView!
    
    
    // Declarations
    fileprivate lazy var fetchedResultsController: NSFetchedResultsController<CDPrice> = {
       
        // Fetch
        let fetchRequest: NSFetchRequest<CDPrice> = CDPrice.fetchRequest()
        let context = CoreDataManager.shared.mainContext
        
        // Sort
        let sortDescriptorTimeStamp = NSSortDescriptor(key: "timeStamp", ascending: false)
        fetchRequest.sortDescriptors = [sortDescriptorTimeStamp]
        
        // Faulting
        fetchRequest.fetchBatchSize = 40
      
        // Preapre Request
        let fetchController = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: context, sectionNameKeyPath: "sectionName", cacheName: nil)
        fetchController.delegate = self
        return fetchController
    }()
    
    private lazy var refreshControl: UIRefreshControl = {
       
        let refresh = UIRefreshControl()
        refresh.tintColor = UIColor("#FFC908")
        refresh.addTarget(self, action: #selector(readUpdatedPriceFromServer), for: UIControlEvents.valueChanged)
        return refresh
    }()
    
    private lazy var emptyView: EmptyView = {
        
        let view = EmptyView(frame: CGRect(x: 0, y: 108, width: 375, height: 24 + 21 + 24))
        return view
    }()
    
    fileprivate let kHeaderHeight: CGFloat = 40
    private let request = NetworkController.shared
    private let urls = URLs.shared
    private let warningMessages = WarningMessages.shared
    private var isLoading = false
    
    
    
    
    // MARK: Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
    
        configureView()
        loadDataFromDataBase()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        registerListeners()
        readUpdatedPriceFromServer()
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        removeObservers()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        resetSubViewFramesIfNeeded()
        navigationItem.updateLogoFrame()
    }
    
    
    
    
    
    
    // MARK: Arrange View
    func configureView() {
        
        // Style & Config table View
        tableView.refreshControl = refreshControl
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 80
        tableView.tableFooterView = UIView()
        
        // Style & Configure Navigation
        navigationController?.navigationBar.isTranslucent = false
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        navigationController?.navigationBar.barTintColor                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    = UIColor("#FFC908")
        navigationController?.navigationBar.tintColor = .white
        
        let rightBarButton = UIBarButtonItem(image: UIImage(named: "refresh"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(readUpdatedPriceFromServer))
        navigationItem.rightBarButtonItem = rightBarButton
        
        headersContainerView.backgroundColor = UIColor("#FFC908")
    }
    
    
    // Reset All the subview frames here
    func resetSubViewFramesIfNeeded() {
        
        emptyView.frame.size.width = view.bounds.width
    }
    
    // Register BroadCast Listeners
    func registerListeners() {
        
        NotificationCenter.default.addObserver(self, selector: #selector(refresh(_:)), name: .kApplicationWillEnterForeground, object: nil)
    }
    
    // Remove all the Broadcast Listeners
    func removeObservers() {
        
        NotificationCenter.default.removeObserver(self)
    }
    
    
    // Fetch Updated Price from server
    @objc func readUpdatedPriceFromServer() {
        
        // Validate Call Queue
        if isLoading {
           return
        }
        
        // Start loading Indication
        loadingView.startAnimate()
        isLoading = true
        
        // Prepare Network
        request.get(urls.kSpotPrice, params: [:]) { (networkStatus, response) in
            
            // Network Validation
            if !networkStatus {
                
                self.view.showToastMessage(self.warningMessages.noconnection)
                self.endLoading()
                return
            }
            
            // Response Validaiton
            if let _response = response as? Dictionary<String, Any> {
                
                // Perform Response data Assign and Save to Database
                _ = SpotPriceModal(_response)
            }
            
            self.endLoading()
        }
        
    }
    
    // Stop All Network Indications
    func endLoading() {
     
        isLoading = false
        tableView.refreshControl?.endRefreshing()
        loadingView.stopAnimate()
    }
    
    // Load Price List from DataBase
    func loadDataFromDataBase() {
        
        try? fetchedResultsController.performFetch()
    }
    
    func showEmptyView() {
     
        tableView.isHidden = true
        if !(emptyView.isDescendant(of: view)) {
        
            view.addSubview(emptyView)
        }
    }
    
    func hideEmptyView() {
        
        tableView.isHidden = false
        if emptyView.isDescendant(of: view) {
        
            emptyView.removeFromSuperview()
        }
    }
    
    // Notification Responder
    @objc func refresh(_ notification: NSNotification) {
        
        readUpdatedPriceFromServer()
    }
}



// Support Fetch Controller
extension PriceListViewController: NSFetchedResultsControllerDelegate {
 
    
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        
        tableView.beginUpdates()
    }
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        
        tableView.endUpdates()
    }
    
    /*
        Every value changes in DataBase is Monitoring by the Folowing Method
        - We expect only New value Insertion in our case
    */
    
    // Section Update
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange sectionInfo: NSFetchedResultsSectionInfo, atSectionIndex sectionIndex: Int, for type: NSFetchedResultsChangeType) {
        
        switch type {
        case .insert:
            tableView.insertSections(IndexSet([sectionIndex]), with: .fade)
            
        default:
            break
        }
    }
    
    // Row in Section Update
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        
        switch type {
            
        case .insert:
            
            // Validate
            guard let _newIndexPath = newIndexPath else {
                return
            }
            tableView.insertRows(at: [_newIndexPath], with: .fade)
            
        default:
            break
        }
    }
}


// Support Table Listing
extension PriceListViewController: UITableViewDataSource, UITableViewDelegate {
  
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        // Validation
        guard let sections = fetchedResultsController.sections, sections.count > 0 else {
            showEmptyView()
            return 0
        }
        hideEmptyView()
        return sections.count
    }
   
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        guard let sections = fetchedResultsController.sections else { return 0 }
        return sections[section].numberOfObjects
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {

        guard let _ = fetchedResultsController.sections else { return 0 }
        return kHeaderHeight
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        // Validation
        guard let sections = fetchedResultsController.sections else { return nil }
        
        // New Header View
        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: view.frame.size.width, height: kHeaderHeight))
        headerView.backgroundColor = UIColor("FAFAFA")
        
        // Title Label
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = sections[section].name
        label.textAlignment = .right
        label.textColor = UIColor("23282D")
        headerView.addSubview(label)
        
        // Add Constraints
        label.leftAnchor.constraint(equalTo: headerView.leftAnchor).isActive = true
        label.rightAnchor.constraint(equalTo: headerView.rightAnchor, constant: -24).isActive = true
        label.centerYAnchor.constraint(equalTo: headerView.centerYAnchor).isActive = true
        label.heightAnchor.constraint(equalToConstant: kHeaderHeight).isActive = true
        
        return headerView
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        // Data Cell
        let cell = tableView.dequeueReusableCell(withIdentifier: PriceListTableViewCell.identifier) as! PriceListTableViewCell
        cell.data = fetchedResultsController.object(at: indexPath)
        return cell
    }
}



